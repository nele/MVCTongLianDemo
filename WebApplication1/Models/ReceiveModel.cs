﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class ReceiveModel
    {
        public String verifySrc { set; get; }
        public bool verifyResult { set; get; }

        public String merchantId { set; get; }
        public String version { set; get; }
        public String language { set; get; }
        public String signType { set; get; }
        public String payType { set; get; }
        public String issuerId { set; get; }
        public String paymentOrderId { set; get; }
        public String orderNo { set; get; }
        public String orderDatetime { set; get; }
        public String orderAmount { set; get; }
        public String payDatetime { set; get; }
        public String payAmount { set; get; }
        public String ext1 { set; get; }
        public String ext2 { set; get; }
        public String payResult { set; get; }
        public String errorCode { set; get; }
        public String returnDatetime { set; get; }
        public String key { set; get; }
        public String signMsg { set; get; }
    }
}