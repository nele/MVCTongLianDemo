﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class PostModel
    {
        public String srcMsg { set; get; }
        public String signMsg { set; get; }

        public String serverUrl { set; get; }

        public String key { set; get; }
        public String version { set; get; }
        public String language { set; get; }
        public String inputCharset { set; get; }
        public String merchantId { set; get; }
        public String pickupUrl { set; get; }
        public String receiveUrl { set; get; }
        public String payType { set; get; }
        public String signType { set; get; }
        public String orderNo { set; get; }
        public String orderAmount { set; get; }
        public String orderDatetime { set; get; }
        public String orderCurrency { set; get; }
        public String orderExpireDatetime { set; get; }
        public String payerTelephone { set; get; }
        public String payerEmail { set; get; }
        public String payerIDCard { set; get; }
        public String payerName { set; get; }
        public String pid { set; get; }
        public String productName { set; get; }
        public String productId { set; get; }
        public String productNum { set; get; }
        public String productPrice { set; get; }
        public String productDesc { set; get; }
        public String ext1 { set; get; }
        public String ext2 { set; get; }
        public String extTL { set; get; }
        public String issuerId { set; get; }
        public String pan { set; get; }
        public String tradeNature { set; get; }
    }
}