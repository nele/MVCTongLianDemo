﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {

        // GET: Home
        public ActionResult Index()
        {
            ClaimsIdentity ci = new ClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie);
            ci.AddClaim(new Claim(ClaimTypes.Name, "Brock"));
            ci.AddClaim(new Claim(ClaimTypes.Role, "Admin"));
            ci.AddClaim(new Claim(ClaimTypes.Email, "brockallen@gmail.com"));
            //var id = new ClaimsIdentity(ci, DefaultAuthenticationTypes.ApplicationCookie, ClaimTypes.Name, ClaimTypes.Role);
            var ctx = HttpContext.GetOwinContext();
            var authenticationManager = ctx.Authentication;
            authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = true }, ci);
            return RedirectToAction("Test1");
            //return View();
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Test1()
        {
            var id = HttpContext.User.Identity as ClaimsIdentity;
            return View();
        }

        [Authorize(Roles = "Root")]
        public ActionResult Test2()
        {
            var id = HttpContext.User.Identity as ClaimsIdentity;
            return View();
        }




        [HttpPost]
        public ActionResult Post(PostModel model)
        {
            StringBuilder bufSignSrc = new StringBuilder();
            appendSignPara(bufSignSrc, "inputCharset", model.inputCharset);
            appendSignPara(bufSignSrc, "pickupUrl", model.pickupUrl);
            appendSignPara(bufSignSrc, "receiveUrl", model.receiveUrl);
            appendSignPara(bufSignSrc, "version", model.version);
            appendSignPara(bufSignSrc, "language", model.language);
            appendSignPara(bufSignSrc, "signType", model.signType);
            appendSignPara(bufSignSrc, "merchantId", model.merchantId);
            appendSignPara(bufSignSrc, "payerName", model.payerName);
            appendSignPara(bufSignSrc, "payerEmail", model.payerEmail);
            appendSignPara(bufSignSrc, "payerTelephone", model.payerTelephone);
            appendSignPara(bufSignSrc, "payerIDCard", model.payerIDCard);
            appendSignPara(bufSignSrc, "pid", model.pid);
            appendSignPara(bufSignSrc, "orderNo", model.orderNo);
            appendSignPara(bufSignSrc, "orderAmount", model.orderAmount);
            appendSignPara(bufSignSrc, "orderCurrency", model.orderCurrency);
            appendSignPara(bufSignSrc, "orderDatetime", model.orderDatetime);
            appendSignPara(bufSignSrc, "orderExpireDatetime", model.orderExpireDatetime);
            appendSignPara(bufSignSrc, "productName", model.productName);
            appendSignPara(bufSignSrc, "productPrice", model.productPrice);
            appendSignPara(bufSignSrc, "productNum", model.productNum);
            appendSignPara(bufSignSrc, "productId", model.productId);
            appendSignPara(bufSignSrc, "productDesc", model.productDesc);
            appendSignPara(bufSignSrc, "ext1", model.ext1);
            appendSignPara(bufSignSrc, "ext2", model.ext2);
            appendSignPara(bufSignSrc, "extTL", model.extTL);
            appendSignPara(bufSignSrc, "payType", model.payType);
            appendSignPara(bufSignSrc, "issuerId", model.issuerId);
            appendSignPara(bufSignSrc, "pan", model.pan);
            appendSignPara(bufSignSrc, "tradeNature", model.tradeNature);
            appendLastSignPara(bufSignSrc, "key", model.key);

            model.srcMsg = bufSignSrc.ToString();
            model.signMsg = FormsAuthentication.HashPasswordForStoringInConfigFile(model.srcMsg, "MD5");
            return View(model);
        }


        [HttpGet]
        public ActionResult OrderPost()
        {
            return View();
        }


        //---------------------------------------以下代码请勿更动------------------------------------------------------------

        private bool isEmpty(String src)
        {
            if (null == src || "".Equals(src) || "-1".Equals(src))
            {
                return true;
            }
            return false;
        }

        private void appendSignPara(System.Text.StringBuilder buf, String key, String value)
        {
            if (!isEmpty(value))
            {
                buf.Append(key).Append('=').Append(value).Append('&');
            }
        }

        private void appendLastSignPara(System.Text.StringBuilder buf, String key,
                String value)
        {
            if (!isEmpty(value))
            {
                buf.Append(key).Append('=').Append(value);
            }
        }
    }
}