﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderPost.aspx.cs" Inherits="WebApplication2.OrderPost" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form action="./Post.aspx" method="post" runat="server">
        <table border="1" cellpadding="1" cellspacing="1" align="center">
            <tr>
                <td>提交地址：</td>
                <td>
                    <input type="text" name="serverUrl" value="http://ceshi.allinpay.com/gateway/index.do" /></td>
            </tr>

            <tr>
                <td>1. 字符集:</td>
                <td>
                    <input type="text" name="inputCharset" value="1" /></td>
            </tr>
            <tr>
                <td>2. 取货地址:</td>
                <td>
                    <input type="text" name="pickupUrl" value="http://localhost:3075/MyWebSite/receive/receive.aspx" /><font color="red">*</font></td>
            </tr>
            <tr>
                <td>3. 商户系统通知地址:</td>
                <td>
                    <input type="text" name="receiveUrl" value="http://localhost:3075/MyWebSite/receive/receive.aspx" /><font color="red">*</font></td>
            </tr>
            <tr>
                <td>4. 版本号:</td>
                <td>
                    <input type="text" name="version" value="v1.0" /><font color="red">*</font></td>
            </tr>
            <tr>
                <td>5. 语言:</td>
                <td>
                    <input type="text" name="language" value="" /></td>
            </tr>
            <tr>
                <td>6. 签名类型:</td>
                <td>
                    <input type="text" name="signType" value="1" /><font color="red">*</font></td>
            </tr>
            <tr>
                <td>7. 商户号:</td>
                <td>
                    <input type="text" name="merchantId" value="100020091218001" /><font color="red">*测试商户号</font></td>
            </tr>
            <tr>
                <td>8. 付款人姓名:</td>
                <td>
                    <input type="text" name="payerName" value="" /></td>
            </tr>
            <tr>
                <td>9. 付款人联系email:</td>
                <td>
                    <input type="text" name="payerEmail" value="" /></td>
            </tr>
            <tr>
                <td>10. 付款人电话:</td>
                <td>
                    <input type="text" name="payerTelephone" value="" /></td>
            </tr>
            <tr>
                <td>11. 付款人证件号:</td>
                <td>
                    <input type="text" name="payerIDCard" value="" /></td>
            </tr>
            <tr>
                <td>12. 合作伙伴商户号:</td>
                <td>
                    <input type="text" name="pid" value="" /></td>
            </tr>
            <tr>
                <td>13. 商户系统订单号:</td>
                <td>
                    <input type="text" name="orderNo" id="orderNo" value="" /><font color="red">*</font><input type="button" value="生成订单号" onclick="setOrderNo()" /></td>
            </tr>
            <tr>
                <td>14. 订单金额(单位分):</td>
                <td>
                    <input type="text" name="orderAmount" value="200" /><font color="red">*</font></td>
            </tr>
            <tr>
                <td>15. 订单金额币种类型:</td>
                <td>
                    <input type="text" name="orderCurrency" value="" /></td>
            </tr>
            <tr>
                <td>16. 商户的订单提交时间:</td>
                <td>
                    <input type="text" name="orderDatetime" id="orderDatetime" value="" /><font color="red">*</font></td>
            </tr>
            <tr>
                <td>17. 订单过期时间:</td>
                <td>
                    <input type="text" name="orderExpireDatetime" value="" /></td>
            </tr>
            <tr>
                <td>18. 商品名称:</td>
                <td>
                    <input type="text" name="productName" value="" /></td>
            </tr>
            <tr>
                <td>19. 商品单价:</td>
                <td>
                    <input type="text" name="productPrice" value="" /></td>
            </tr>
            <tr>
                <td>20. 商品数量:</td>
                <td>
                    <input type="text" name="productNum" value="" /></td>
            </tr>
            <tr>
                <td>21. 商品标识:</td>
                <td>
                    <input type="text" name="productId" value="" /></td>
            </tr>
            <tr>
                <td>22. 商品描述:</td>
                <td>
                    <input type="text" name="productDesc" value="" /></td>
            </tr>
            <tr>
                <td>23. 扩展字段1:</td>
                <td>
                    <input type="text" name="ext1" value="" /></td>
            </tr>
            <tr>
                <td>24. 扩展字段2:</td>
                <td>
                    <input type="text" name="ext2" value="" /></td>
            </tr>
            <tr>
                <td>25. 业务扩展字段:</td>
                <td>
                    <input type="text" name="extTL" value="" /></td>
            </tr>
            <tr>
                <td>26. 支付方式:</td>
                <td>
                    <input type="text" name="payType" value="0" /><font color="red">*</font></td>
            </tr>
            <tr>
                <td>27. 发卡方代码:</td>
                <td>
                    <input type="text" name="issuerId" value="" /><span style="font-size: 12px; color: blue;"></span></td>
            </tr>
            <tr>
                <td>28. 付款人支付卡号:</td>
                <td>
                    <input type="text" name="pan" value="" /></td>
            </tr>
            <tr>
                <td>29. 贸易类型:</td>
                <td>
                    <input type="text" name="tradeNature" value="" /></td>
            </tr>
            <tr>
                <td>用于计算signMsg的key值:</td>
                <td>
                    <input type="text" name="key" id="Text3" value="1234567890" /><font color="red">*</font></td>
            </tr>
        </table>
        <div align="center">
            <input type="submit" value="提交表单，计算signMsg" />
        </div>
    </form>
    <script type="text/javascript">
        setOrderNo();
        function setOrderNo() {
            var curr = new Date();
            var m = curr.getMonth() + 1;
            if (m < 10) { m = '0' + m; }
            var d = curr.getDate();
            if (d < 10) { d = '0' + d; }
            var h = curr.getHours();
            if (h < 10) { h = '0' + h; }
            var mi = curr.getMinutes();
            if (mi < 10) { mi = '0' + mi; }
            var s = curr.getSeconds();
            if (s < 10) { s = '0' + s; }
            var strDatetime = '' + curr.getFullYear() + m + d + h + mi + s;
            document.getElementById("orderDatetime").value = strDatetime;
            document.getElementById("orderNo").value = 'NO' + strDatetime;
        }
    </script>
</body>
</html>
