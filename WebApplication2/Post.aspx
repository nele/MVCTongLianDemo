﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Post.aspx.cs" Inherits="WebApplication2.Post" %>

 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>通联网上支付平台-商户接口范例-支付提交</title>
</head>
<body>
     <center> <h2>通联支付网关接口支付提交</h2></center>
    <center> 订单提交地址：<%=serverUrl%></center>
    <form id="form1" action="<%=serverUrl%>" method="post">    
    <table border="1" cellpadding="1" cellspacing="1" align="center">
   		<tr><td>编码</td><td><input type="text" name="inputCharset" id="inputCharset" value="<%=inputCharset%>" readonly/></td></tr>
		<tr><td>取货地址</td><td><input type="text" name="pickupUrl" id="serverUrl" value="<%=pickupUrl%>" readonly/></td></tr>
		<tr><td>通知地址</td><td><input type="text" name="receiveUrl" id="receiveUrl" value="<%=receiveUrl %>" readonly/></td></tr>
		<tr><td>版本号</td><td><input type="text" name="version" id="version" value="<%=version %>" readonly/></td></tr>
		<tr><td>语言</td><td><input type="text" name="language" id="language" value="<%=language %>" readonly/></td></tr>
		<tr><td>签名方式</td><td><input type="text" name="signType" id="signType" value="<%=signType %>" readonly/></td></tr>
		<tr><td>商户号</td><td><input type="text" name="merchantId" id="merchantId" value="<%=merchantId %>" readonly/></td></tr>
		<tr><td>付款人名称</td><td><input type="text" name="payerName" id="payerName" value="<%=payerName %>" readonly/></td></tr>
		<tr><td>付款人邮件</td><td><input type="text" name="payerEmail" id="payerEmail" value="<%=payerEmail %>" readonly/></td></tr>
		<tr><td>付款人电话</td><td><input type="text" name="payerTelephone" id="payerTelephone" value="<%=payerTelephone %>" readonly/></td></tr>
		<tr><td>付款人证件号</td><td><input type="text" name="payerIDCard" id="payerIDCard" value="<%=payerIDCard %>" readonly/></td></tr>
		<tr><td>合作伙伴商户号</td><td><input type="text" name="pid" id="pid" value="<%=pid %>" readonly/></td></tr>
		<tr><td>订单号</td><td><input type="text" name="orderNo" id="orderNo" value="<%=orderNo %>" readonly/></td></tr>
		<tr><td>订单金额</td><td><input type="text" name="orderAmount" id="orderAmount" value="<%=orderAmount %>" readonly/></td></tr>
		<tr><td>金额币种</td><td><input type="text" name="orderCurrency" id="orderCurrency" value="<%=orderCurrency %>" readonly/></td></tr>
		<tr><td>订单时间</td><td><input type="text" name="orderDatetime" id="orderDatetime" value="<%=orderDatetime %>" readonly/></td></tr>
		<tr><td>订单过期时间</td><td><input type="text" name="orderExpireDatetime" id="orderExpireDatetime" value="<%=orderExpireDatetime %>" readonly/></td></tr>
		<tr><td>商品名称</td><td><input type="text" name="productName" id="productName" value="<%=productName %>" readonly/></td></tr>
		<tr><td>商品单价</td><td><input type="text" name="productPrice" id="productPrice" value="<%=productPrice %>" readonly/></td></tr>
		<tr><td>商品数量</td><td><input type="text" name="productNum" id="productNum" value="<%=productNum %>" readonly/></td></tr>
		<tr><td>商品代码</td><td><input type="text" name="productId" id="productId" value="<%=productId %>" readonly/></td></tr>
		<tr><td>商品描述</td><td><input type="text" name="productDesc" id="productDesc" value="<%=productDesc %>" readonly/></td></tr>
		<tr><td>附加参数1</td><td><input type="text" name="ext1" id="ext1" value="<%=ext1 %>" readonly/></td></tr>
		<tr><td>附加参数2</td><td><input type="text" name="ext2" id="ext2" value="<%=ext2 %>" readonly/></td></tr>
		<tr><td>业务扩展字段</td><td><input type="text" name="extTL" id="extTL" value="<%=extTL %>" readonly/></td></tr>
		<tr><td>支付方式</td><td><input type="text" name="payType" id="payType" value="<%=payType %>" readonly/></td></tr>
		<tr><td>发卡行机构号</td><td><input type="text" name="issuerId" id="issuerId" value="<%=issuerId %>" readonly/></td></tr>
		<tr><td>付款人支付卡号</td><td><input type="text" name="pan" id="pan" value="<%=pan %>" readonly/></td></tr>
		<tr><td>贸易类型</td><td><input type="text" name="tradeNature" id="tradeNature" value="<%=tradeNature %>" readonly/></td></tr>
		<tr><td>签名串</td><td><input type="text" name="signMsg" id="signMsg" value="<%=signMsg %>" readonly/></td></tr>
		<tr><td colspan="2"><input type="submit" name="确认支付" value="信息正确，确认付款"/></td></tr>
    </table>
	<br/>	
    </form>
</body>
</html>
