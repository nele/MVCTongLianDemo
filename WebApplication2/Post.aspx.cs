﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class Post : System.Web.UI.Page
    {
        public String srcMsg = null;
        public String signMsg = null;

        public String serverUrl;

        public String key;
        public String version;
        public String language;
        public String inputCharset;
        public String merchantId;
        public String pickupUrl;
        public String receiveUrl;
        public String payType;
        public String signType;
        public String orderNo;
        public String orderAmount;
        public String orderDatetime;
        public String orderCurrency;
        public String orderExpireDatetime;
        public String payerTelephone;
        public String payerEmail;
        public String payerIDCard;
        public String payerName;
        public String pid;
        public String productName;
        public String productId;
        public String productNum;
        public String productPrice;
        public String productDesc;
        public String ext1;
        public String ext2;
        public String extTL;
        public String issuerId;
        public String pan;
        public String tradeNature;

        protected void Page_Load(object sender, EventArgs e)
        {
            serverUrl = Request.Form["serverUrl"];

            key = Request.Form["key"];
            version = Request.Form["version"];
            language = Request.Form["language"];
            inputCharset = Request.Form["inputCharset"];
            merchantId = Request.Form["merchantId"];
            pickupUrl = Request.Form["pickupUrl"];
            receiveUrl = Request.Form["receiveUrl"];
            payType = Request.Form["payType"];
            signType = Request.Form["signType"];
            orderNo = Request.Form["orderNo"];
            orderAmount = Request.Form["orderAmount"];
            orderDatetime = Request.Form["orderDatetime"];
            orderCurrency = Request.Form["orderCurrency"];
            orderExpireDatetime = Request.Form["orderExpireDatetime"];
            payerTelephone = Request.Form["payerTelephone"];
            payerEmail = Request.Form["payerEmail"];
            payerName = Request.Form["payerName"];
            payerIDCard = Request.Form["payerIDCard"];
            pid = Request.Form["pid"];
            productName = Request.Form["productName"];
            productId = Request.Form["productId"];
            productNum = Request.Form["productNum"];
            productPrice = Request.Form["productPrice"];
            productDesc = Request.Form["productDesc"];
            ext1 = Request.Form["ext1"];
            ext2 = Request.Form["ext2"];
            extTL = Request.Form["extTL"];
            issuerId = Request.Form["issuerId"];
            pan = Request.Form["pan"];
            tradeNature = Request.Form["tradeNature"];

            StringBuilder bufSignSrc = new StringBuilder();
            appendSignPara(bufSignSrc, "inputCharset", inputCharset);
            appendSignPara(bufSignSrc, "pickupUrl", pickupUrl);
            appendSignPara(bufSignSrc, "receiveUrl", receiveUrl);
            appendSignPara(bufSignSrc, "version", version);
            appendSignPara(bufSignSrc, "language", language);
            appendSignPara(bufSignSrc, "signType", signType);
            appendSignPara(bufSignSrc, "merchantId", merchantId);
            appendSignPara(bufSignSrc, "payerName", payerName);
            appendSignPara(bufSignSrc, "payerEmail", payerEmail);
            appendSignPara(bufSignSrc, "payerTelephone", payerTelephone);
            appendSignPara(bufSignSrc, "payerIDCard", payerIDCard);
            appendSignPara(bufSignSrc, "pid", pid);
            appendSignPara(bufSignSrc, "orderNo", orderNo);
            appendSignPara(bufSignSrc, "orderAmount", orderAmount);
            appendSignPara(bufSignSrc, "orderCurrency", orderCurrency);
            appendSignPara(bufSignSrc, "orderDatetime", orderDatetime);
            appendSignPara(bufSignSrc, "orderExpireDatetime", orderExpireDatetime);
            appendSignPara(bufSignSrc, "productName", productName);
            appendSignPara(bufSignSrc, "productPrice", productPrice);
            appendSignPara(bufSignSrc, "productNum", productNum);
            appendSignPara(bufSignSrc, "productId", productId);
            appendSignPara(bufSignSrc, "productDesc", productDesc);
            appendSignPara(bufSignSrc, "ext1", ext1);
            appendSignPara(bufSignSrc, "ext2", ext2);
            appendSignPara(bufSignSrc, "extTL", extTL);
            appendSignPara(bufSignSrc, "payType", payType);
            appendSignPara(bufSignSrc, "issuerId", issuerId);
            appendSignPara(bufSignSrc, "pan", pan);
            appendSignPara(bufSignSrc, "tradeNature", tradeNature);
            appendLastSignPara(bufSignSrc, "key", key);

            srcMsg = bufSignSrc.ToString();
            signMsg = FormsAuthentication.HashPasswordForStoringInConfigFile(srcMsg, "MD5");
        }


        //---------------------------------------以下代码请勿更动------------------------------------------------------------

        private bool isEmpty(String src)
        {
            if (null == src || "".Equals(src) || "-1".Equals(src))
            {
                return true;
            }
            return false;
        }

        private void appendSignPara(System.Text.StringBuilder buf, String key, String value)
        {
            if (!isEmpty(value))
            {
                buf.Append(key).Append('=').Append(value).Append('&');
            }
        }

        private void appendLastSignPara(System.Text.StringBuilder buf, String key,
                String value)
        {
            if (!isEmpty(value))
            {
                buf.Append(key).Append('=').Append(value);
            }
        }
    }
}